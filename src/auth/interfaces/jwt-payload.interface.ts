export interface JwtPayload {
  username: string;
  email: string;
  coins: number;
}