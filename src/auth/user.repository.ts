import { User } from './models/user.entity';
import {Repository,EntityRepository } from 'typeorm';
import { InternalServerErrorException, ConflictException } from '@nestjs/common';
import { RegisterDto } from './dto/register.dto';
import * as bcrypt from 'bcryptjs';
import { LoginDto } from './dto/login.dto';
@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async signUp(authCreds : RegisterDto) : Promise<void>{
    const {username,password,email} = authCreds;
    const user = new User();
    user.salt = await bcrypt.genSalt();
    user.username = username;
    user.email = email;
    user.coins = 100;
    user.password = await this.hashPassword(password,user.salt);
    try{
        await user.save();
    }catch(err){
        if(err.code == '23505'){
            throw new ConflictException("A user with this username already exists");
        }
        else{
            throw new InternalServerErrorException();
        }
    }
    
}
private async hashPassword(password : string, salt : string) : Promise<string>{
    return bcrypt.hash(password,salt);
}
public async validatePassword(authCreds : LoginDto) : Promise<User>{
    const {password,email} = authCreds;
    const user = await this.findOne({email});
    if(user && await user.validatePassword(password)){
        return user;
    }else {
        return null;
    }

}
}