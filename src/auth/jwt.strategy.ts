import { UnauthorizedException } from "@nestjs/common";
import { JwtPayload } from "./interfaces/jwt-payload.interface";
import { User } from "./models/user.entity";
import { UserRepository } from "./user.repository";
import { InjectRepository } from "@nestjs/typeorm";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from 'passport-jwt';
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(@InjectRepository(UserRepository) private userRepository : UserRepository) {
      super({
          jwtFromRequest : ExtractJwt.fromAuthHeaderAsBearerToken(),
          secretOrKey : 'qdadazds546as5d4adaz874!',
      });
  }
  async validate(payload : JwtPayload) : Promise<User>{
      const {email} = payload;
      const user = this.userRepository.findOne({email});
      if(!user){
          throw new UnauthorizedException();
      }
      return user;
  }
}