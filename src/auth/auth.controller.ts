import { Controller, Post, Body, ValidationPipe, Put, UseGuards } from '@nestjs/common';
import { RegisterDto } from './dto/register.dto';
import { AuthService } from './auth.service';
import { AuthResultDto } from './dto/auth-result.dto';
import { LoginDto } from './dto/login.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { GetUser } from './get-user.decorator';
import { User } from './models/user.entity';
import { AuthGuard } from '@nestjs/passport';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) { }
  @Post("/signup")
  signUp(@Body(ValidationPipe) authCreds: RegisterDto): Promise<void> {
    return this.authService.signUp(authCreds);
  }
  @Post("/signin")
  signIn(@Body(ValidationPipe) authCreds: LoginDto): Promise<AuthResultDto> {
    console.log(authCreds);
    return this.authService.signIn(authCreds);
  }
  @UseGuards(AuthGuard())
  @Put()
  updateCoins(@GetUser() user: User): Promise<UpdateUserDto>{
    user.coins += 20;
    return this.authService.updateUserCoins(user);
  }
}
