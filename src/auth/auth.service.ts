import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { JwtService } from '@nestjs/jwt';
import { RegisterDto } from './dto/register.dto';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { AuthResultDto } from './dto/auth-result.dto';
import { LoginDto } from './dto/login.dto';
import { User } from './models/user.entity';
import { UpdateUserDto } from './dto/update-user.dto';
@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository, private jwtService: JwtService) { }
  async signUp(authCreds: RegisterDto): Promise<void> {
    return this.userRepository.signUp(authCreds);
  }
  async signIn(authCreds: LoginDto): Promise<AuthResultDto> {
    const user = await this.userRepository.validatePassword(authCreds);
    if (!user) {
      throw new UnauthorizedException("Bad credentials");
    }
    let payload: JwtPayload = {
      email: user.email,
      username: user.username,
      coins : user.coins
    };
    let accessToken = await this.jwtService.sign(payload);
    let authResult: AuthResultDto = new AuthResultDto();
    authResult.coins = user.coins;
    authResult.email = user.email;
    authResult.username = user.username;
    authResult.token = accessToken;
    authResult.exp = new Date();
    authResult.exp.setHours(authResult.exp.getHours() + 1);
    return authResult;
  }
  async updateUserCoins(user: User): Promise<UpdateUserDto>
  {
    user.email = undefined;
    user.username = undefined;
    user.password = undefined;
    user.salt = undefined;
    let updatedUser = await this.userRepository.save(user);
    let updatedUserDto = new UpdateUserDto();
    updatedUserDto.coins = updatedUser.coins;
    updatedUserDto.email = updatedUser.email;
    updatedUserDto.username = updatedUser.username;
    return updatedUserDto;
  }
}
