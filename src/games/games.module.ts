import { Module } from '@nestjs/common';
import { AuthModule } from 'src/auth/auth.module';
import { GamesController } from './games.controller';
import { AuthService } from 'src/auth/auth.service';

@Module({
  imports: [AuthModule],
  controllers: [GamesController]
})
export class GamesModule {
}
