import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { User } from 'src/auth/models/user.entity';
import { GetUser } from '../auth/get-user.decorator';
import { UserRepository } from 'src/auth/user.repository';
import { AuthService } from '../auth/auth.service';
@UseGuards(AuthGuard())
@Controller('games')
export class GamesController {
    constructor(private authService : AuthService) { }
  @Get()
  getOneGame(@GetUser() user : User) { 
        const reel1 : number[] =  [1, 2, 4, 2, 3, 3, 2, 2];
        const reel2 : number[] = [2, 4, 2, 2, 1, 4, 3, 2];
        const reel3 : number[] =  [2, 4, 2, 4, 1, 2, 3, 2];
        const rand1 : number = Math.floor(Math.random()* Math.floor(8));
        const rand2 : number = Math.floor(Math.random()* Math.floor(8));
        const rand3 : number = Math.floor(Math.random()* Math.floor(8));
        const result : number[]  =[reel1[rand1],reel2[rand2],reel3[rand3]];
      let coins = user.coins - 1;
      if (coins < 0) { 
        return {"result" : result,"coins" : 0};
      }
        if(result[0]==result[1] && result[0] == result[2])
        {
            switch(result[0])
            {
                case 1:
                    coins+=50;
                break;
                case 2:
                    coins+=3;
                break;
                case 3:
                    coins+=15;
                    break;
                case 4:
                    coins+=20;
                    break;
            }
        }
        else if(result[0]==result[1] || result[0] == result[2])
        {
            switch(result[0])
            {
                case 1:
                    coins+=40;
                break;
                case 3:
                    coins+=5;
                    break;
                case 4:
                    coins+=10;
                    break;
            }
        }
        else if(result[1] == result[2])
        {
            switch(result[1])
            {
                case 1:
                    coins+=40;
                break;
                case 3:
                    coins+=5;
                    break;
                case 4:
                    coins+=10;
                    break;
            }
        }
        user.coins = coins;
        this.authService.updateUserCoins(user);
        return {"result" : result,"coins" : coins};
  }
}
