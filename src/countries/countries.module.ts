import { Module, HttpModule } from '@nestjs/common';
import { CountriesController } from './countries.controller';
import { CountriesService } from './countries.service';

@Module({
  imports : [HttpModule],
  controllers: [CountriesController],
  providers: [CountriesService]
})
export class CountriesModule {}
