import { Controller, Get, Query, Param, NotFoundException, UsePipes, ValidationPipe } from '@nestjs/common';
import { CountriesService } from './countries.service';
import { GetCountriesDto } from './dto/get-countries.dto';
import { Country } from './country.model';
@Controller('countries')
export class CountriesController {
    constructor(private countriesService : CountriesService)
    {

    }
    @Get()
    @UsePipes(new ValidationPipe({transform: true}))
    getCountries(@Query() getDto : GetCountriesDto) : Promise<Country[]> {
        return this.countriesService.findAll(getDto).then((res) => {return res;},(err) => {throw new NotFoundException();});
    }
    @Get('/:name')
    getCountryByName(@Param('name') name : string) : Promise<Country> {
        return this.countriesService.findOne(name).then((res) => {return res;},(err) => {throw new NotFoundException();});
    }

}
