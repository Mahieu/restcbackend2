import { IsArray, IsOptional } from 'class-validator';
import { Transform } from 'class-transformer';
export class GetCountriesDto {
    @IsArray()
    @IsOptional()
    @Transform(names => JSON.parse(names), {toClassOnly: true})
    names : string[];
}