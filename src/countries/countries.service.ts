import { Injectable, HttpService } from '@nestjs/common';
import { GetCountriesDto } from './dto/get-countries.dto';
import { Country } from './country.model';
@Injectable()
export class CountriesService {
    constructor(private httpService : HttpService){}
    async findAll(params : GetCountriesDto) : Promise<Country[]> {
        let response;
        if(Array.isArray(params.names))
        {
            let data : Country[] = [];
            for (let i = 0; i < params.names.length; i++) {
                try{
                    response = await this.httpService.get('https://restcountries.eu/rest/v2/name/'+params.names[i]).toPromise();
                }
                catch(err)
                {
                    throw new Error('Country not found');
                }
                if(response.status==200)
                {
                    data = data.concat(response.data);
                }
            }
            return data.filter((v,i,a)=>a.findIndex(t=>(t.name === v.name))===i);
        }
        else
        {
            try{
                const res = await this.httpService.get('https://restcountries.eu/rest/v2/').toPromise();
                if(res.status == 200)
                {
                    return res.data;
                }
            }catch(err)
            {
                throw new Error('Country not found');
            }
        }
    }
    async findOne(name : string) : Promise<Country>
    {
        let response;
        try{
            response = await this.httpService.get('https://restcountries.eu/rest/v2/name/'+name+'?fulltext=true').toPromise();
        }
        catch(err)
        {
            throw new Error('Country not found');
        }
        if(response.status == 200)
        {
            return response.data[0];
        }
        throw new Error('Country not found');
    }
}
