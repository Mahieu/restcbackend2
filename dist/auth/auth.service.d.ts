import { UserRepository } from './user.repository';
import { JwtService } from '@nestjs/jwt';
import { RegisterDto } from './dto/register.dto';
import { AuthResultDto } from './dto/auth-result.dto';
import { LoginDto } from './dto/login.dto';
import { User } from './models/user.entity';
import { UpdateUserDto } from './dto/update-user.dto';
export declare class AuthService {
    private userRepository;
    private jwtService;
    constructor(userRepository: UserRepository, jwtService: JwtService);
    signUp(authCreds: RegisterDto): Promise<void>;
    signIn(authCreds: LoginDto): Promise<AuthResultDto>;
    updateUserCoins(user: User): Promise<UpdateUserDto>;
}
