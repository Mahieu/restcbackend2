import { BaseEntity } from "typeorm";
export declare class User extends BaseEntity {
    id: number;
    username: string;
    password: string;
    email: string;
    coins: number;
    salt: string;
    validatePassword(password: string): Promise<boolean>;
}
