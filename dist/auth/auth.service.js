"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_repository_1 = require("./user.repository");
const jwt_1 = require("@nestjs/jwt");
const auth_result_dto_1 = require("./dto/auth-result.dto");
const update_user_dto_1 = require("./dto/update-user.dto");
let AuthService = class AuthService {
    constructor(userRepository, jwtService) {
        this.userRepository = userRepository;
        this.jwtService = jwtService;
    }
    async signUp(authCreds) {
        return this.userRepository.signUp(authCreds);
    }
    async signIn(authCreds) {
        const user = await this.userRepository.validatePassword(authCreds);
        if (!user) {
            throw new common_1.UnauthorizedException("Bad credentials");
        }
        let payload = {
            email: user.email,
            username: user.username,
            coins: user.coins
        };
        let accessToken = await this.jwtService.sign(payload);
        let authResult = new auth_result_dto_1.AuthResultDto();
        authResult.coins = user.coins;
        authResult.email = user.email;
        authResult.username = user.username;
        authResult.token = accessToken;
        authResult.exp = new Date();
        authResult.exp.setHours(authResult.exp.getHours() + 1);
        return authResult;
    }
    async updateUserCoins(user) {
        user.email = undefined;
        user.username = undefined;
        user.password = undefined;
        user.salt = undefined;
        let updatedUser = await this.userRepository.save(user);
        let updatedUserDto = new update_user_dto_1.UpdateUserDto();
        updatedUserDto.coins = updatedUser.coins;
        updatedUserDto.email = updatedUser.email;
        updatedUserDto.username = updatedUser.username;
        return updatedUserDto;
    }
};
AuthService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_repository_1.UserRepository)),
    __metadata("design:paramtypes", [user_repository_1.UserRepository, jwt_1.JwtService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map