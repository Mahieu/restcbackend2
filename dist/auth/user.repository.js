"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepository = void 0;
const user_entity_1 = require("./models/user.entity");
const typeorm_1 = require("typeorm");
const common_1 = require("@nestjs/common");
const bcrypt = require("bcryptjs");
let UserRepository = class UserRepository extends typeorm_1.Repository {
    async signUp(authCreds) {
        const { username, password, email } = authCreds;
        const user = new user_entity_1.User();
        user.salt = await bcrypt.genSalt();
        user.username = username;
        user.email = email;
        user.coins = 100;
        user.password = await this.hashPassword(password, user.salt);
        try {
            await user.save();
        }
        catch (err) {
            if (err.code == '23505') {
                throw new common_1.ConflictException("A user with this username already exists");
            }
            else {
                throw new common_1.InternalServerErrorException();
            }
        }
    }
    async hashPassword(password, salt) {
        return bcrypt.hash(password, salt);
    }
    async validatePassword(authCreds) {
        const { password, email } = authCreds;
        const user = await this.findOne({ email });
        if (user && await user.validatePassword(password)) {
            return user;
        }
        else {
            return null;
        }
    }
};
UserRepository = __decorate([
    typeorm_1.EntityRepository(user_entity_1.User)
], UserRepository);
exports.UserRepository = UserRepository;
//# sourceMappingURL=user.repository.js.map