import { RegisterDto } from './dto/register.dto';
import { AuthService } from './auth.service';
import { AuthResultDto } from './dto/auth-result.dto';
import { LoginDto } from './dto/login.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './models/user.entity';
export declare class AuthController {
    private authService;
    constructor(authService: AuthService);
    signUp(authCreds: RegisterDto): Promise<void>;
    signIn(authCreds: LoginDto): Promise<AuthResultDto>;
    updateCoins(user: User): Promise<UpdateUserDto>;
}
