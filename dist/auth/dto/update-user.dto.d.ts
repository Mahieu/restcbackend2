export declare class UpdateUserDto {
    email: string;
    username: string;
    coins: number;
}
