export declare class AuthResultDto {
    token: string;
    username: string;
    email: string;
    coins: number;
    exp: Date;
}
