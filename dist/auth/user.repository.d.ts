import { User } from './models/user.entity';
import { Repository } from 'typeorm';
import { RegisterDto } from './dto/register.dto';
import { LoginDto } from './dto/login.dto';
export declare class UserRepository extends Repository<User> {
    signUp(authCreds: RegisterDto): Promise<void>;
    private hashPassword;
    validatePassword(authCreds: LoginDto): Promise<User>;
}
