import { HttpService } from '@nestjs/common';
import { GetCountriesDto } from './dto/get-countries.dto';
import { Country } from './country.model';
export declare class CountriesService {
    private httpService;
    constructor(httpService: HttpService);
    findAll(params: GetCountriesDto): Promise<Country[]>;
    findOne(name: string): Promise<Country>;
}
