import { CountriesService } from './countries.service';
import { GetCountriesDto } from './dto/get-countries.dto';
import { Country } from './country.model';
export declare class CountriesController {
    private countriesService;
    constructor(countriesService: CountriesService);
    getCountries(getDto: GetCountriesDto): Promise<Country[]>;
    getCountryByName(name: string): Promise<Country>;
}
