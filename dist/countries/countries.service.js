"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CountriesService = void 0;
const common_1 = require("@nestjs/common");
let CountriesService = class CountriesService {
    constructor(httpService) {
        this.httpService = httpService;
    }
    async findAll(params) {
        let response;
        if (Array.isArray(params.names)) {
            let data = [];
            for (let i = 0; i < params.names.length; i++) {
                try {
                    response = await this.httpService.get('https://restcountries.eu/rest/v2/name/' + params.names[i]).toPromise();
                }
                catch (err) {
                    throw new Error('Country not found');
                }
                if (response.status == 200) {
                    data = data.concat(response.data);
                }
            }
            return data.filter((v, i, a) => a.findIndex(t => (t.name === v.name)) === i);
        }
        else {
            try {
                const res = await this.httpService.get('https://restcountries.eu/rest/v2/').toPromise();
                if (res.status == 200) {
                    return res.data;
                }
            }
            catch (err) {
                throw new Error('Country not found');
            }
        }
    }
    async findOne(name) {
        let response;
        try {
            response = await this.httpService.get('https://restcountries.eu/rest/v2/name/' + name + '?fulltext=true').toPromise();
        }
        catch (err) {
            throw new Error('Country not found');
        }
        if (response.status == 200) {
            return response.data[0];
        }
        throw new Error('Country not found');
    }
};
CountriesService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [common_1.HttpService])
], CountriesService);
exports.CountriesService = CountriesService;
//# sourceMappingURL=countries.service.js.map