"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GamesController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const user_entity_1 = require("../auth/models/user.entity");
const get_user_decorator_1 = require("../auth/get-user.decorator");
const user_repository_1 = require("../auth/user.repository");
const auth_service_1 = require("../auth/auth.service");
let GamesController = class GamesController {
    constructor(authService) {
        this.authService = authService;
    }
    getOneGame(user) {
        const reel1 = [1, 2, 4, 2, 3, 3, 2, 2];
        const reel2 = [2, 4, 2, 2, 1, 4, 3, 2];
        const reel3 = [2, 4, 2, 4, 1, 2, 3, 2];
        const rand1 = Math.floor(Math.random() * Math.floor(8));
        const rand2 = Math.floor(Math.random() * Math.floor(8));
        const rand3 = Math.floor(Math.random() * Math.floor(8));
        const result = [reel1[rand1], reel2[rand2], reel3[rand3]];
        let coins = user.coins - 1;
        if (coins < 0) {
            return { "result": result, "coins": 0 };
        }
        if (result[0] == result[1] && result[0] == result[2]) {
            switch (result[0]) {
                case 1:
                    coins += 50;
                    break;
                case 2:
                    coins += 3;
                    break;
                case 3:
                    coins += 15;
                    break;
                case 4:
                    coins += 20;
                    break;
            }
        }
        else if (result[0] == result[1] || result[0] == result[2]) {
            switch (result[0]) {
                case 1:
                    coins += 40;
                    break;
                case 3:
                    coins += 5;
                    break;
                case 4:
                    coins += 10;
                    break;
            }
        }
        else if (result[1] == result[2]) {
            switch (result[1]) {
                case 1:
                    coins += 40;
                    break;
                case 3:
                    coins += 5;
                    break;
                case 4:
                    coins += 10;
                    break;
            }
        }
        user.coins = coins;
        this.authService.updateUserCoins(user);
        return { "result": result, "coins": coins };
    }
};
__decorate([
    common_1.Get(),
    __param(0, get_user_decorator_1.GetUser()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_entity_1.User]),
    __metadata("design:returntype", void 0)
], GamesController.prototype, "getOneGame", null);
GamesController = __decorate([
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.Controller('games'),
    __metadata("design:paramtypes", [auth_service_1.AuthService])
], GamesController);
exports.GamesController = GamesController;
//# sourceMappingURL=games.controller.js.map