import { User } from 'src/auth/models/user.entity';
import { AuthService } from '../auth/auth.service';
export declare class GamesController {
    private authService;
    constructor(authService: AuthService);
    getOneGame(user: User): {
        result: number[];
        coins: number;
    };
}
