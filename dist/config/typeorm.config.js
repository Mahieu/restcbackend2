"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.typeOrmConfig = void 0;
exports.typeOrmConfig = {
    type: 'postgres',
    port: 5432,
    url: process.env.DATABASE_URL || 'postgres://sveygfxtkpmhmt:11227e91b580c693db21ce3b031ec3f73af9a8c17f9a45a05f6f3df191f67618@ec2-54-75-150-32.eu-west-1.compute.amazonaws.com:5432/dfpuo9ino7g86m',
    entities: [__dirname + '/../**/*.entity.{js,ts}'],
    synchronize: true,
    ssl: true,
    extra: {
        ssl: {
            rejectUnauthorized: false,
        },
    }
};
//# sourceMappingURL=typeorm.config.js.map