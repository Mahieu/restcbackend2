# How to run the website

## Step 1

Install NodeJS LTS : https://nodejs.org/en/download/

## Step 2

Open a terminal and go inside the folder of the project (restcountriesbackend).

## Step 3

Run this command : 

npm install

## Step 4

Run this command : 

npm start

## Step 5

Open your favorite web browser and use this URL to access the website : 

localhost:4000

## Explanations

### General

When you arrive on the website, you've got a navbar on top of the screen.

There is a link for each question.

### Question 1

You need to type a country name in the search bar and then click on the search button to get a description of the country.

### Question 2

You need to type a partial name or a full name and then click on the add button, and finally, click on the search button.

If you want to remove a search term, you can click on the badge to remove it from the list.

### Question 3

You need to type something to filter the countries, it's filtered based on the name.

### Question 4

I saw that you were using PixiJS to create your games on yobetit, so I took a look and remade the game in PixiJS.

This time, I did implement a JWT Authentification, so you need to login before playing the game.

To log in, just click on the link in the navbar and then, click on the login button and go back to this question.

To play, just press on the play button.

## Online version

https://restcountriesbackend.herokuapp.com/